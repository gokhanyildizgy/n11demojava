package tests;

import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.SearchPage;

public class e2eTest extends BaseTest {


    @Test (description = "Aramada Sonucu Sayısı Kontrolü")
    public void e2e() throws InterruptedException {

        //*************PAGE INSTANTIATIONS*************
        HomePage homePage = new HomePage(driver, wait);
        LoginPage loginPage = new LoginPage(driver, wait);
        SearchPage searchPage = new SearchPage(driver,wait);

        //*************PAGE Methods********************
        //Open N11 HomePage
        homePage.goToN11();

        //Accept KVKK
        homePage.acceptKVKKAlert();

        //Go to LoginPage
        homePage.goToLoginPage();

        //Login to N11
        loginPage.loginToN11("seleniumDemo1111@gmail.com", "denemeGg123");

        //*************ASSERTIONS***********************
        Thread.sleep(1500);
//        //Verify Login
//        loginPage.verifyLogin(("Selen Yum"));

        loginPage.goToHomepage();

        //Search
        homePage.search("Samsung");

        //Check Result Count
        searchPage.getResultCount();

        //Verify URL
        searchPage.checkURL("https://www.n11.com/arama?q=Samsung");

        //Click Category
        searchPage.clickTelCategory();

        //Check Result Count
        searchPage.getResultCount();

    }
}