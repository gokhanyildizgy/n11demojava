package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class SearchPage extends BasePage {

    //*********Constructor*********
    public SearchPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    //*********Web Elements*********
    String breadCrumb = "breadCrumb";

    String resultCount = "#contentListing > div > div > div.productArea > section > div.header > div.resultText > strong";

    String telCategory = "#contentListing > div > div > div.filterArea > section.filter.filterCategory > ul > li > ul > li > ul:nth-child(2) > li > a";

    String successfulSeller = "successfulSellerOption";

    //*********Page Methods*********

    public void verifySearch(String expectedKeyword) {

        Assert.assertTrue(readText(By.id(breadCrumb)).contains(expectedKeyword));
    }

    public void getResultCount(){
        String count = driver.findElement(By.cssSelector(resultCount)).getText();
        count = count.replace(",", "");
        System.out.println(count);
        Integer countInt = Integer.parseInt(count);
        Assert.assertTrue(countInt > 0);
    }

    public void checkURL(String url) {
        String currentURL = driver.getCurrentUrl();
        Assert.assertTrue(currentURL.contentEquals(url));
    }

    public void clickTelCategory() {
        sleep();
        driver.findElement(By.partialLinkText("Telefon")).click();
    }

    public void clickSuccessfulSellerOption() {
        click(By.id(successfulSeller));
    }

    public void verifyBadgeSuccess() {
        Assert.assertTrue(driver.findElement(By.className("badgeSuccess")).isDisplayed());
    }
}