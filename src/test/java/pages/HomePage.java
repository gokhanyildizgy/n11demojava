package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    //*********Constructor*********
    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    //*********Page Variables*********
    String baseURL = "http://www.n11.com/";

    //*********Web Elements*********
    String signInButtonClass = "btnSignIn";

    String searchBox = "searchData";

    String searchBtn = "searchBtn";

    String KVKKAcceptButton = "//span[@class='btn btnBlack']";

    //*********Page Methods*********

    //Go to Homepage
    public void goToN11() {
        driver.get(baseURL);
        //driver.navigate().to(baseURL)
    }

    //Accept KVKK Alert
    public void acceptKVKKAlert () throws InterruptedException {
        click(By.xpath(KVKKAcceptButton));
        Thread.sleep(1000);
    }

    //Go to LoginPage
    public void goToLoginPage() {
        click(By.className(signInButtonClass));
    }

    //Search
    public void search(String keyword) {
        writeText(By.id(searchBox), keyword);
        click(By.className(searchBtn));

    }
}